import numpy as np			# DATA STRUCTURE
from math import log		

worthless_gain = 0.1

def computeEntropy(samplesPerClass,totalSamples):
		entropies = [-(samples/float(totalSamples))*log(samples/float(totalSamples),2) for samples in samplesPerClass]		
		return np.sum(entropies)

class TreeNode:
	# FIXME: treat continuous values
	
	def __init__(self,featureName,trainSet,entropy):
		
#		print("Creating '{}' node...".format(featureName))
		
		self.name = featureName		
		self.trainSet = trainSet
		self.entropy = entropy
		
	def __str__(self):
		return "<{}>".format(self.name)
			
	def __repr__(self):
		return "<{}>".format(self.name)
	
	#### TRAIN ####
	def train(self):
		
		print("*** Training '{}' node ***".format(self.name))
		
		values = self.__getValues()
		possibleValues = list(set(values))
		self.edges = [[x,None] for x in possibleValues]															
	
#		print(possibleValues)
						
		currentIndex = np.argwhere(self.trainSet[0] == self.name)[0][0]		
		trainSetByValue = [self.trainSet[self.trainSet[:,currentIndex] == value].copy() for value in possibleValues]				
		
		if self.trainSet[0].size > 2:									
			for index,trainSet in enumerate(trainSetByValue):				
				trainSetByValue[index] = np.delete(trainSet,currentIndex,axis=1) 																				
		header = np.delete(self.trainSet[0],currentIndex)
		
		for index,edge in enumerate(self.edges):
			nodes = []				
			
#			print(">> Edge {}".format(edge[0]))			

			# Split train set by edge value
			edgeTrainSet = trainSetByValue[index]						
			edgeTrainSet = np.insert(edgeTrainSet,0,header,axis=0)

			# ENTROPY
			targets = edgeTrainSet[1:,-1]			
			samplesPerTarget = [targets[targets == target].shape[0] for target in set(targets)]
			N = targets.shape[0]
			entropy = computeEntropy(samplesPerTarget,N)				
			
			for featureName in edgeTrainSet[0,:-1]:																
				# Create new node
				nodes.append(TreeNode(featureName,edgeTrainSet,entropy))

			# attach the best node
			gains = [node.getInfoGain() for node in nodes]
#			print(self.entropy)
#			print(gains)
				
			highestGainNodeIndex = np.argmax(gains)
		
			if gains[highestGainNodeIndex] < worthless_gain or gains[highestGainNodeIndex] == 0:
				target = nodes[highestGainNodeIndex].trainSet[1,-1]				
				edge[1] = LeafNode(target)
				continue
			
			self.edges[index][1] = nodes[highestGainNodeIndex]
			
		print(self.edges)						
		
		if self.trainSet[0].size <= 2:
			print("End of training!")
		else:
			for edge in self.edges:
				if edge[1] != None:
					edge[1].train()

	
	#### INFO GAIN ####
	def getInfoGain(self):				
		values = self.__getValues()
		targets = self.trainSet[1:,-1]				
	
		totalSamples = len(values)
		possibleValues = list(set(values))
		
		proportions = np.array([values[values == value].size/float(totalSamples) for value in possibleValues])
		
		infoGains = np.array([self.__getInfoGainForValue(value) for value in possibleValues])		
#		print(infoGains)

		return self.entropy - sum(proportions*infoGains)
		
	def __getInfoGainForValue(self,selectedValue):	
		values = self.__getValues()
		targets = self.trainSet[1:,-1]			
		
		shape = (len(values),1)
		v = np.reshape(values,shape)
		t = np.reshape(targets,shape)
		vt = np.concatenate((v,t),axis=1)

		vt = vt[vt[:,0] == selectedValue]
		
		samplesPerTargetClass = [vt[vt[:,1] == target].shape[0] for target in set(targets)]
		samplesPerTargetClass = np.trim_zeros(samplesPerTargetClass)
		N = vt.shape[0] 
		
		entropies = [-(samples/float(N))*log(samples/float(N),2) for samples in samplesPerTargetClass]
#		print(entropies)
		return np.sum(entropies)
	
	def __getValues(self):
		featureIndex = list(self.trainSet[0]).index(self.name)					
		return self.trainSet[1:,featureIndex]	
	
	def classify(self, instance):
		
		# Choose next edge
		index = list(self.trainSet[0]).index(self.name)			
		nextIndex = instance[index]
		instance = np.delete(instance,index)
		
		print("\nSelected node: {} \nAttribute value: {}".format(self.name,nextIndex))				
		print("Available nodes: {}\n".format(self.edges))		
		
		for index,edge in enumerate(self.edges):			
			if edge[0] == nextIndex:
				nextNode = self.edges[index][1]				
				return nextNode.classify(instance)
		return "[ERROR] Undefined class"
	
class RootNode(TreeNode):
	def __init__(self,trainSet,featureName="Root",isDebug=False):
#		print("*** Creating '{}' node ***".format(featureName))
		self.name = featureName
		self.trainSet = trainSet
		self.entropy = 1
		self.isDebug = isDebug

	def train(self):
		
		# ENTROPY
		targets = self.trainSet[1:,-1]			
		samplesPerTarget = [targets[targets == target].shape[0] for target in set(targets)]
		N = targets.shape[0]
		self.entropy = computeEntropy(samplesPerTarget,N)
		
		nodes = []
		
		for featureName in self.trainSet[0,:-1]:			
			nodes.append(TreeNode(featureName,self.trainSet,self.entropy))		
			
		gains = [node.getInfoGain() for node in nodes]
		
		if self.isDebug:
			print("\nGains at root level\n{}".format(gains))
		
		highestGainNodeIndex = np.argmax(gains)			
		self.edges = [[nodes[highestGainNodeIndex].name,nodes[highestGainNodeIndex]]]
		
		nodes[highestGainNodeIndex].train()
		
	def classify(self,instance):
		return self.edges[0][1].classify(instance)
	
	
class LeafNode(TreeNode):
	def __init__(self, value):
#		print("*** Creating Leaf node ***")		
		self.name = value

	def classify(self,instance):
		return self.name
	
	def train(self):
		pass