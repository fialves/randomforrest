#! /usr/bin/python

import csv
import math
from sys import argv as argv
from sys import exit as exit

import numpy as np			# DATA STRUCTURE

from tree import Tree 

# STEP 5: split bootstraps and create our random forrest
# STEP 6: implement majority votting and classify an instance
# STEP 7: implement cross-validation and fit number of trees
# STEP 8: enable continuous values features			

CATEGORICAL = 0
NUMERIC = 1

dt = np.dtype([('name', np.unicode_, 16), ('grades', np.float64, (2,))])


cmc_header = np.array([["Wife_age",NUMERIC],
				   ["Wife_education",CATEGORICAL],
				   ["Husband_education",CATEGORICAL],
				   ["Total_children",NUMERIC],
				   ["Wife_religion",CATEGORICAL],
				   ["Wife_isEmployed",CATEGORICAL],
				   ["Husband_occupation",CATEGORICAL],
				   ["Standard_of_living",CATEGORICAL],
				   ["Media_exposure",CATEGORICAL],
				   ["Contraceptive_method_used",CATEGORICAL]]) # TODO: add resultant class

#				np.dtype([["Wife_age",float],
#				   ["Wife_education",str],
#				   ["Husband_education",str],
#				   ["Total_children",float],
#				   ["Wife_religion",str],
#				   ["Wife_isEmployed",str],
#				   ["Husband_occupation",str],
#				   ["Standard_of_living",str],
#				   ["Media_exposure",str],
#				   ["Contraceptive_method_used",str]]) # TODO: add resultant class
						

wine_header = np.dtype([("Alcohol",float),
				   ("Malic_acid",float),
				   ("Ash",float),
				   ("Alcalinity_of_ash",float),
				   ("Magnesium",float),
				   ("Total_phenols",float),
				   ("Flavanoids",float),
				   ("Nonflavanoid_phenols",float),
				   ("Proanthocyanins",float),
				   ("Color_intensity",float),
				   ("Hue",float),
				   ("Diluted_wines",float),
				   ("Proline",float),
				   ("Type",str)])

#				np.array([["Alcohol",NUMERIC],
#				   ["Malic_acid",NUMERIC],
#				   ["Ash",NUMERIC],
#				   ["Alcalinity_of_ash",NUMERIC],
#				   ["Magnesium",NUMERIC],
#				   ["Total_phenols",NUMERIC],
#				   ["Flavanoids",NUMERIC],
#				   ["Nonflavanoid_phenols",NUMERIC],
#				   ["Proanthocyanins",NUMERIC],
#				   ["Color_intensity",NUMERIC],
#				   ["Hue",NUMERIC],
#				   ["Diluted_wines",NUMERIC],
#				   ["Proline",NUMERIC],
#				   ["Type",CATEGORICAL]])

haberman_header  = np.array([["Age",NUMERIC],
   				    ["Year_of_operation",NUMERIC],
   					["Positive_nodes",NUMERIC],
   					["Survived_after5",CATEGORICAL] ]) # TODO: add resultant class

def preprocessing(dataset,header):
	print("Dataset length is {}".format(dataset.shape))
	print("Header length is {}".format(len(header)))
	
	dataset.astype(header)
	
#	for index,attribute in enumerate(header):
#		if attribute[1] == CATEGORICAL:
#			dataset[:,index].astype("string")
#		attribute = np.delete(attribute,1)
	
	header = header.flatten()
	dataset = np.insert(dataset,0,header)
	
	

if __name__ == "__main__":		
	filename = argv[1]
	with open(filename) as csvfile:		
		dataset = []
		ext = filename.split('.')[1]		
		if ext == "csv":
			dataset = list(csv.reader(csvfile, delimiter=';', quotechar='|'))		
		elif ext == "data":
			dataset = list(csv.reader(csvfile, delimiter=',', quotechar='|'))	
		else:
			print("Unrecognized file format")
			exit()
		
		tree = Tree()
		
		if len(argv) > 2 and argv[2] == 'corretude':
			tree.train(dataset,True)
			exit()
		
		# add headers to all trainSets, but benchmark		
		if filename != "benchmark.csv":
			print('\n*** Insert header ***')
			header = []
			if filename == "datasets/cmc.data" :				
				header = cmc_header
			elif filename == "datasets/wine.data" :
				header = wine_header
			elif filename == "datasets/haberman.data" :
				header = haberman_header

			dataset = np.array(dataset)
			preprocessing(dataset,header)												
		
		print('\n*** Prepare for Cross Validation (separating in 80/20 proportion) ***')
		total_rows = len(dataset) - 1 # less header		
		bound = int(math.floor(total_rows * 0.8))
		trainset = dataset[0:bound]
		testset = dataset[bound:total_rows]

		print('\n*** Training ***')		
		tree.train(trainset)
		
		# NOTE: Should it outputs a trained forrest?
		
		print('\n\n*** Classifying ***')		
#		testset = np.array(testset)
		print("Instance: {}\n".format(testset))
		for sample in testset:
			result = tree.classify(sample)			
			print("=> Instance classified as {}".format(result))

	
	
# 1. split dataset in bootstraps

# 2. initialize each tree with a bootstrap 

# 3. train all trees

# 4. all trees evaluate each instance 

# 5. apply majority vote algorithm for each instance

# 6. cross-validation (80% train, 20% test, stratified) to fit n (#trees) using accuracy and m (#features). NOTE.: m can be square root of total features