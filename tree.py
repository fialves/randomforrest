import numpy as np			# DATA STRUCTURE
from treeNode import TreeNode, RootNode


class Tree:		
		
	def train(self, trainSet,isDebug=False):		
		trainSet = np.array(trainSet)													
		self.root = RootNode(trainSet,isDebug)
		self.root.train()

	def classify(self, instance):	
		return self.root.classify(instance)